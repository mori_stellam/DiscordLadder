# Insurgency Community League Ladder Bot

ICL community is hosting a ladder for Teams through discord using the bot. This manual explains the rules of the ladder and the use of the bot to play in the ladder.

The purpose of the ladder is to facilitate active teams and provide continuous challenge to the teams. Teams can sign up to the ladder, challenge other teams also in the ladder, and win or lose points based on the results of those matches. The resulting points will determined the ranking of the teams on the ladder. This concept is similar to that of ESL, so that to get a basic concept of what a ladder is, you can read up on https://play.eslgaming.com/ladder-faq. Here we will only focus on the ICL specific details: how to use the bot, and what are the (planned) rules of the ladder.

## How to use the bot?

Before going into specifics, the bot uses `slash commands`. After you type the command, the additional informations (parameters) you need to provide is highlighted above the command.

### Team Management

First, all commands for team management can only be used in the `#teamrosters` channel.

To sign up to the ladder, you need the create a team using the `/createteam` command (in the team roster channel). This command will create a team with the issuer of the command as the captain and with a team name that you specify with the command. You can also indicate the region of the team with flags (e.g. 🇪🇺 (`:flag_eu:`),🇺🇸 (`:flag_us:`)) specified in the command. So for example you create a team named ABC with :flag_eu: with `/createteam ABC :flag_eu:`. Note a few points about creating teams:

- You receive a random ID number which you will need to use later on.
- You cannot specify a team name that is already in use.
- The team can only participate in the ladder (get rank and can challenge other teams) once it has at least 5 eligible players.

Once you have created a team as a captain, you can invite other players to the team via `/inviteplayer` command. You will need to specify the player you invite by pinging that person on Discord (using pinging `@` - so for example: `/inviteplayer @DISCORDUSER`). Then the person you have invited can either accept or reject the invitation. In case you want to revoke your invitation, the captain can also click on "Reject". About inviting users:

- You can only invite users who is not connected to another team.
- The maximum number of players connected to the team is maximum 7.
- By connected, the bot means either having been invited to the team and already being a member to the team.
- Once the invited player has accepted the invitation, there is a _10 day waiting period until_ the player becomes eligible to play matches with the team. During this 10 days, the player also cannot be removed neither by the team captain, nor the by the player themselves.
- Only the captain of the team can manage the roster using the `/inviteplayer @DISCORDUSER` and the `/kickplayer @DISCORDUSER` commands. Note that you CANNOT kick yourself from the team as captain (see commands for assigning a new captain or disbanding the team).
- Players can also remove themselves from the team with the `/leaveteam` command.

Once you have enough eligible players in your team, your team will appear in the `#ranking` channel and you as a captain can start challenging other teams.

If as a captain you wish to promote another player in the team to be the new captain of the team, you can use the `/changeCaptain @DISCORDUSER`. The other player you refer must be already be a member of the team.

Finally, if you wish to disband the team, you can issue the `/disbandTeam` command as captain. In this case, all players will be kicked from the team and the team becomes inactive. Note you can only disband a team once you have no active challenges or active matches.

### Matches

To start a ranked match between teams, you as a captain first needs to challenge another team. To do this, you need to find the `TEAMID` of other team in the `#ranking` channel, then go to the `#matches` channel, and issue the `/challenge TEAMID` command. Then the captain of the other command will be pinged by the bot to accept or to reject the challenge. If you decide to cancel the challenge, you can also press the `Reject` button. To be able to challenge another team, there are a few conditions:

- Both teams must have at least 5 eligible players.
- Neither teams can have more than 3 challenges or active matches.
- The difference between the ranks of the teams should not be larger than a threshold. _(at the moment disabled, but will be activated later)_

If the challenged team accepts the challenge, then the match enters into the map picking phase. Starting with the captain who started the challenge, the captains take turns to ban one map. The ban will continue until there are 3 maps or less and until the two captains banned the same amount of maps. The bot will then randomly pick a map from the pool of remaining maps. You can then continue to play the match.

After the match has been played, both captains will then have to report the outcome by pushing the same button. This applies if the match has been cancelled. If the two captains report contradicting results, then evidence must be submitted to admins to resolve the issue. Otherwise, if both captains report the same results, then the bot continues to evaluate the outcome of the match and update the ELO and ranking of the teams in the `#ranking` channel.

_Note: The bot uses `trueskill` package to evaluate team ELOs after the match. It takes the ELO points of the teams before the match and calculates the expected outcome of the match. Then the new ranking after the match is updated based on whether the outcome was expected or not. ELO-s are stored at the player level, so that when a player changes teams, the ELO of the old and the new teams are likely to be changed. You can find more details about the `trueskill` system following https://trueskill.org/ and https://en.wikipedia.org/wiki/TrueSkill._

## Commands / Code reference

**Captain commands**
- `/createteam TEAMNAME TEAMFLAG` in channel `#teamrosters`- Creates team with a given name and assigns a given flag (etc: `/createteam ABC :flag_eu:`).
- `/inviteplayer @DISCORDUSER` in channel `#teamrosters` - Invites the given discord user to the team.
- `/kickplayer @DISCORDUSER` in channel `#teamrosters` - Kicks the @DISCORDUSER from the team if the user is part of the team. No changes otherwise.
- `/changeCaptain @DISCORDUSER` in channel `#teamrosters` - Assigns @DISCORDUSER to be the new captain of the team if @DISCORDUSER is already an eligible team member.
- `/disbandTeam` in channel `#teamrosters` - Disband team once the team has no active challenges and active matches.
- `/challenge TEAMID` in channel `#matches` - Starts a ranked match by challenging the other team with ID `TEAMID`. You can find team IDs in the `#ranking` channel.

**Player commands**
- `/leaveteam` in channel `#teamrosters` - Players leave the current team.

**Admin commands**
- `/setAdminRole @DISCORDROLE` in ANY channel - To specify which role should be considered as admin by the bot.
- `/initladdertable` - Initializes the `#ranking` channel in the channel where the command is issued. Nothing changes if a channel has been initialized.
- `/initteamroster` - Initializes the `#teamrosters` channel in the channel where the command is issued. Nothing changes if a channel has been initialized.
- `/initMatchesChannel` - Initializes the `#matches` channel in thge channel where the command is issued. Nothing changes if a channel has been initialized.
- `/updateLadderTable` in channel `#ranking` - Updates the ranking table (only utility function).
- `/updateTeamRosters` in channel `#teamrosters` - Updates team rosters (only utility function).
- `/addPlayerToTeam @DISCORDUSER` in channel `#teamrosters` - Assigns @DISCORDUSER to the team if conditions hold.
- `/updatesql "DB_TABLENAME" "DB_SETEXPRESSION" "DB_CONDITION_EXPRESSION"` in ANY channel - Manually changes the values of certain rows in the specified tables. More specifically, the command issues the following SQL line: `UPDATE {DB_TABLENAME} SET {DB_SETEXPRESSION} WHERE {DB_CONDITION_EXPERSSSION}`. Only use this command if you are familiar with SQL and know the structure of the SQLITE database.
