import configparser

parsedParams = configparser.ConfigParser()
parsedParams.read('config.ini')

# Database and discord parameters
dbname = parsedParams["DATABASE"]["name"]

# Team defaults
defTScore = int(parsedParams["TEAMPARAMS"]["defaultScore"])
defTElo = int(parsedParams["TEAMPARAMS"]["defaultElo"])
defTMaxPlay = 7
defTFixDays = int(parsedParams["TEAMPARAMS"]["defTFixDays"])
defMaxMatches = int(parsedParams["TEAMPARAMS"]["defMaxMatches"])

# Player default
defPElo = parsedParams["PLAYERPARAMS"]["defaultPlayerElo"]

# DCCONFIG
TimeSleep = int(parsedParams["DCCONFIG"]["timesleep"])
LadderInt =  int(parsedParams["DCCONFIG"]["LadderInt"])
LadderChannelID = int(parsedParams["DCCONFIG"]["LadderChannelID"])
TeamChannelID = int(parsedParams["DCCONFIG"]["TeamChannelID"])
LadderTeamPerMsg = int(parsedParams["DCCONFIG"]["LadderTeamPerMsg"])
TeamRostTeampPerMsg = int(parsedParams["DCCONFIG"]["LadderTeamPerMsg"])
MatchesChannelID = int(parsedParams["DCCONFIG"]["MatchesChanellID"])
AdminRole = int(parsedParams["DCCONFIG"]["adminRole"])
ChallengeRankRange = int(parsedParams["DCCONFIG"]["ChallengeRankRange"])


# MAPLIST - from maps.txt
with open("maps.txt", "r") as mapfile:
    MapList = [themap.strip() for themap in mapfile.read().split("\n")]