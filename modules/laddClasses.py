import trueskill
from modules import laddConfig

class Team:
    def __init__(self, *args):
        if len(args)==1 and isinstance(args[0], dict):
            values = [v for k, v in args[0].items()]
            self.constParms(*values)
        elif len(args)>=3:
            self.constParms(*args)
        else:
            print("Error in Team constructor.")
        return None

    def constParms(self, pID: int, pName: str, pCpt: int, 
                pWin: int=0, pDraw: int=0, pLoss: int=0,
                pScore: float=laddConfig.defTScore, pElo: float=laddConfig.defTElo,
                pLoc='', pladders: str="", pActiv: int=1):
        self.teamid = pID
        self.name = pName
        self.captain = pCpt
        self.win = pWin 
        self.draw = pDraw
        self.loss = pLoss
        self.score = pScore
        self.elo = pElo
        self.loc = pLoc
        #self.ladders = pladders
        self.activ = pActiv
        return None

    def getQueryStr(self):
        """ Return query string of object
        """
        keys = "(" + ','.join([key for key in self.__dict__.keys()]) +  ")"
        values = "(" + \
                ','.join(["'"+str(key)+"'" for key in self.__dict__.values()]) + \
                ")"
        return [keys, values]


class Player:
    def  __init__(self, *args):
        """ Parameters: playerid: int; name: text; teamid: int; joindate: string; activ: integer(1/0)
        """
        if len(args)==1 and isinstance(args[0], dict):
            values = [v for k, v in args[0].items()]
        elif len(args)>=2:
            values = args
        else:
            print("Error in Team constructor.")
        self.constParms(*values)
        return None

    def constParms(self, pid, pname, ptid=0, pjoind='', pelo=laddConfig.defPElo, 
                        pactiv=1):
        self.playerid = pid
        self.name = pname
        self.teamid = ptid
        self.joindate = pjoind
        self.elo = pelo
        self.activ = pactiv
        return None
    
    def getQueryStr(self):
        """ Return query string of object
        """
        keys = "(" + ','.join([key for key in self.__dict__.keys()]) +  ")"
        values = "(" + \
                ','.join(["'"+str(key)+"'" for key in self.__dict__.values()]) + \
                ")"
        return [keys, values]

    def decodeEloString(self, dbstring: str):
        [mu, sigma] = dbstring.split(":")
        return trueskill.Rating(mu=float(mu), sigma=float(sigma))
    
    def getEloRating(self):
        [mu, sigma] = self.elo.split(":")
        return trueskill.Rating(mu=float(mu), sigma=float(sigma))

    def encodeEloString(self, rating: trueskill.Rating):
        return f"{rating.mu}:{rating.sigma}"


class Match:
    def __init__(self, *args):
        """
            Parameters: matchid: int; team1: int; team2: int;
                        score1: int; score2: int; result: int (1/0/-1);
                        preelo1: integer, preelo2: integer,
                        eloc1: int; eloc2; status: integer, 
                        cdate: text, date: text, map: text, activ: int(1/0)
        """
        if len(args)==1 and isinstance(args[0], dict):
            values = [v for k, v in args[0].items()]
        elif len(args)>=3:
            values = args
        else:
            print("Error in Team constructor.")
        self.constParms(*values)
        return None
        
    def constParms(self, pmid, pt1, pt2, 
                    ps1=0, ps2=0, pr=0, preelo1=0, preelo2=0,
                    pelc1=0, pelc2=0,pstat=1, pcdate='', pdate='', pmap='', pact=1):
        self.matchid = pmid
        self.team1 = pt1
        self.team2 = pt2
        self.score1 = ps1
        self.score2 = ps2
        self.result = pr
        self.preelo1 = preelo1
        self.preelo2 = preelo2
        self.eloc1 = pelc1
        self.eloc2 = pelc2
        self.status = pstat
        self.cdate = pcdate
        self.date = pdate
        self.map = pmap
        self.activ = pact
        return None
    
    def getQueryStr(self):
        """ Return query string of object
        """
        keys = "(" + ','.join([key for key in self.__dict__.keys()]) +  ")"
        values = "(" + \
                ','.join(["'"+str(key)+"'" for key in self.__dict__.values()]) + \
                ")"
        return [keys, values]