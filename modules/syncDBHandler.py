import aiosqlite
import asyncio
import functools
import  logging
import os
from datetime import datetime
from modules import laddClasses, laddConfig
logger = logging.getLogger('DCBOT')


# -----------------------------------------------
# Functions to establish connection with database
# -----------------------------------------------
async def dbinit_initTables(con: aiosqlite.Connection):
    """ Function to create the tables to the newly initialized db
    """

    await con.execute("""CREATE TABLE teams
                    (teamid integer, name text, captain playerid, win integer, draw integer, 
                    loss integer, score real, elo real, loc text, activ integer)
                """) # Teamid - PKey, Captain - Fkey
    await con.execute("""CREATE TABLE matches
                    (matchid integer, team1 integer, team2 integer, score1 integer, score2 integer, 
                    result integer, preelo1 integer, preelo2 integer, eloc1 integer, 
                    eloc2 integer, status integer, cdate text, date text, map text,
                    activ integer)
                """) # Matchid - Pkey, Team1/2 - Fkey
    await con.execute("""CREATE TABLE players
                    (playerid integer, name text, teamid integer, joindate text, elo text, activ integer)
                """) # Playerid - PKey, Teamid integer
    await con.commit()
    return None

def dbdict_factory(cursor: aiosqlite.Cursor, row):
    """ Support function for dbinit_connection to return dictionaries from queries
    """
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

async def dbinit_connection(name: str, reinit: bool=False):
    """ Function to return sqlite connection
    """
    if reinit==True:
        os.remove(name)
    flag = os.path.isfile(name)
    con = await aiosqlite.connect(name)
    con.row_factory = dbdict_factory
    if not flag:
        print("Init db file")
        await dbinit_initTables(con)
    return con

# Decorator for all functionalities
# - Context manager to handle connection generation
# - Decorator both for functions and coroutines
from contextlib import asynccontextmanager
@asynccontextmanager
async def dbConnectContext(name: str):
    logging.info(f"{name}")
    con = await dbinit_connection(name, reinit=False)
    try:
        yield con
    finally:
        await con.close()

def dbopenconnect_decor(f):
    @functools.wraps(f)
    async def wrapped_func(*args, **kwargs):
        async with dbConnectContext(laddConfig.dbname) as conn:
            if not asyncio.iscoroutinefunction(f):
                return f(conn, *args, **kwargs)
            else:
                return await f(conn, *args, **kwargs)
    return wrapped_func


# -------------------------------------------
# Team functions
# -------------------------------------------
@dbopenconnect_decor
async def queryLeaderboard(lcon, laddername: str=""):
    """ Return leaderboard for display. """
    lcurs = await lcon.execute(f"""SELECT * FROM teams WHERE activ='1' ORDER BY elo DESC""")
    returnList = None
    if laddername != "":
        returnlist = [laddClasses.Team(row) for row in await lcurs.fetchall() if laddername in laddClasses.Team(row).ladders]
    else:
        returnlist = [laddClasses.Team(row) for row in await lcurs.fetchall()]
    return returnlist


@dbopenconnect_decor
async def db_addTeam(lcon, team: laddClasses.Team):
    """ Add new team to SQL
    """
    lcurs = await lcon.execute(f"""SELECT * FROM teams 
                    WHERE (name='{team.name}'
                    OR captain='{team.captain}')
                    AND activ='1'
                    ORDER BY elo DESC, name ASC;
                    """)
    if len(await lcurs.fetchall())==0:        
        [keys, values] = team.getQueryStr()
        await lcon.execute(f"""INSERT INTO teams {keys} VALUES {values};""")
        await lcon.commit()
    else:
        print("MSG: Team name already exists or captain is already in another team.")
    return None


@dbopenconnect_decor
async def db_getTeam(lcon, ID: int):
    """ Get team object based on ID
    """
    lcurs = await lcon.execute(f" SELECT * FROM teams  WHERE teamid='{ID}' AND activ='1'")
    fetch = await lcurs.fetchall()
    if len(fetch)>0:
        returnTeam = laddClasses.Team(fetch[0])
    else:
        returnTeam = None
        print("MSG: Could not find team ID.")
    return returnTeam


@dbopenconnect_decor
async def db_getTeamByName(lcon, teamname: str):
    """ Get team object based on team name
    """
    lcurs = await lcon.execute(f" SELECT * FROM teams  WHERE name='{teamname}' AND activ='1'")
    fetch = await lcurs.fetchall()
    if len(fetch)>0:
        returnTeam = laddClasses.Team(fetch[0])
    else:
        returnTeam = None
        print("MSG: Could not find team ID.")
    return returnTeam


@dbopenconnect_decor
async def db_checkTeamName(lcon, tname: str):
    """ Check if team name is in use or not."""
    lcurs = await lcon.execute(f"""SELECT * FROM teams
                                    WHERE name='{tname}' AND activ='1'""")
    fetch = await lcurs.fetchall()
    if len(fetch)==0:
        return False
    else:
        return True


@dbopenconnect_decor
async def db_remTeam(lcon, team: laddClasses.Team, remScore: bool=False):
    """ Remove team from active status
    """
    flag = True
    lcurs = await lcon.execute(f""" SELECT * FROM teams
                        WHERE (name='{team.name}'
                        AND teamid='{team.teamid}')
                        AND activ='1'""")
    fetch = await lcurs.fetchall()
    if remScore:
        scoretxt = f" , win='0', loss='0', score = '{laddConfig.defTScore}', elo = '{laddConfig.defTElo}' "
    else:
        scoretxt = ""
    if len(fetch)!=1:
        flag = False
        print("MSG: Could not find the specified team. Maybe it is already inactive.")
    lcurs = await lcon.execute(f""" SELECT * FROM matches
                                    WHERE (team1='{team.teamid}' OR team2='{team.teamid}')
                                    AND status<4
                                    AND activ='1'""")
    if len(await lcurs.fetchall())>0:
        flag = False
        print("MSG: Cannot remove team in active ladder match.")
    if flag:
        await lcon.execute(f""" UPDATE teams
                            SET activ = '0' , captain = '0' {scoretxt}
                            WHERE (name='{team.name}'
                            AND teamid='{team.teamid}');""")
        await lcon.execute(f""" UPDATE players
                            SET teamid = '0'
                            WHERE teamid = '{team.teamid}'""")
        await lcon.commit()
    return None


@dbopenconnect_decor
async def db_checkPlayerCaptain(lcon, player: laddClasses.Player):
    """ Checks if player is a captain or not. """
    lcurs = await lcon.execute(f"""SELECT * FROM teams WHERE captain='{player.playerid}' """)
    return len(await lcurs.fetchall())>0


@dbopenconnect_decor
async def db_newTeamCapt(lcon, team: laddClasses.Team, player: laddClasses.Player):
    if not await db_checkPlayerCaptain(player):
        lcurs = await lcon.execute(f"""UPDATE teams
                        SET captain='{player.playerid}' 
                        WHERE teamid='{team.teamid}' and activ='1' """)
        await lcon.commit()
    else:
        print("MSG: Player is already captain in another team.")
    return None


@dbopenconnect_decor
async def db_getTeamRoster(lcon, team: laddClasses.Team):
    """ Returns team roster in list starting with the captain """
    lcurs = await lcon.execute(f"""SELECT * FROM players
                        WHERE playerid='{team.captain}'
                        AND teamid='{team.teamid}' 
                        AND joindate!=''
                        AND activ='1' """)
    fetch = await lcurs.fetchall()
    Roster = [ laddClasses.Player(fetch[0]) ]
    lcurs = await lcon.execute(f"""SELECT * FROM players
                        WHERE teamid='{team.teamid}'
                        AND joindate!=''
                        AND activ='1' """)
    for row in await lcurs.fetchall():
        if laddClasses.Player(row).playerid != Roster[0].playerid:
            Roster.append(laddClasses.Player(row))
    return Roster


@dbopenconnect_decor
async def db_updateTeam(lcon, team: laddClasses.Team):
    """ Updating team score based on reported team standing. """
    lcurs = await lcon.execute(f"""
            UPDATE teams
            SET captain='{team.captain}', win='{team.win}', loss='{team.loss}', draw='{team.draw}',
                elo='{team.elo}', score='{team.score}', activ='{team.activ}'
            WHERE teamid='{team.teamid}'
        """)
    await lcon.commit()
    return None


# -------------------------------------------------
# Player functions
# -------------------------------------------------
@dbopenconnect_decor
async def db_addPlayer(lcon, player: laddClasses.Player):
    lcurs = await lcon.execute(f"""SELECT * FROM players
                                    WHERE playerid='{player.playerid}'""")
    fetch = await lcurs.fetchall()
    if len(fetch)==0:
        [keys, values] = player.getQueryStr()
        await lcon.execute(f"""INSERT INTO players {keys} VALUES {values}; """)
        await lcon.commit()
    else:
        dbPlayer = laddClasses.Player(fetch[0])
        if dbPlayer.activ==0:
            dbPlayer.teamid=0
            dbPlayer.joindate=''
            dbPlayer.activ=1
            await db_updatePlayer(dbPlayer)
        else:
            print("MSG: Player is already in DB. Use other methods to update a player.")
    return None


@dbopenconnect_decor
async def db_getPlayerID(lcon, player: laddClasses.Player):
    lcurs = await lcon.execute(f""" SELECT * FROM players WHERE name='{player.name}' """)
    fetch = await lcurs.fetchall()
    if len(fetch)>0:
        playerID = laddConfig.Player(fetch[0]).playerid
    return playerID


@dbopenconnect_decor
async def db_updatePlayer(lcon, player: laddClasses.Player):
    dbPlayer = await db_getPlayer(player.playerid)
    if dbPlayer is None:
        print(f"Player not in database. Cannot update player.")
    else:
        lcurs = await lcon.execute(f""" UPDATE players
                                    SET name='{player.name}', teamid='{player.teamid}',
                                        joindate='{player.joindate}', elo='{player.elo}',
                                        activ='{player.activ}'
                                    WHERE playerid='{player.playerid}' """)
        await lcon.commit()
    return None


@dbopenconnect_decor
async def db_addPlayerToTeam(lcon, player: laddClasses.Player, team: laddClasses.Team):
    dateFlag = True
    maxcountFlag = True
    thisDay = datetime.today()
    if player.joindate!="":
        joinDate = datetime.strptime(player.joindate, "%d-%m-%y")
        delta = thisDay -joinDate
        if delta.days<laddConfig.defTFixDays:
            print("MSG: Cannot join to a team within 2 weeks of joining another team.")
            dateFlag = False
    if dateFlag and maxcountFlag:
        await lcon.execute(f""" UPDATE players 
                            SET teamid='{team.teamid}', activ='1' , 
                                joindate='{str(thisDay.strftime("%d-%m-%y"))}'
                            WHERE playerid='{player.playerid}' """)
        await lcon.commit()
    return None

@dbopenconnect_decor
async def db_remPlayerFromTeam(lcon, player: laddClasses.Player):
    lcurs = await lcon.execute(f""" SELECT * FROM players
                        WHERE playerid='{player.playerid}' 
                        AND activ='1' """)
    fetch = await lcurs.fetchall()
    flag = True
    if len(fetch)>0:
        if laddClasses.Player(fetch[0]).teamid!=0:
            lcurs = await lcon.execute(f"""SELECT * FROM teams 
                            WHERE captain='{player.playerid}' 
                            AND activ='1' """)
            fetch = await lcurs.fetchall()
            if len(fetch):
                flag = False
                teamname = laddClasses.Team(fetch[0]).name
                print("MSG: Player cannot be removed from team being a captain. " + \
                    "Remove team or switch captains.")
        else:
            flag = False
            print("MSG: Player does not have a team.")
    else:
        flag = False
        print("MSG: Could not find active player for removal.")
    if flag==True:
        await lcon.execute(f"""UPDATE players 
                        SET teamid='0' , joindate=''
                        WHERE playerid='{player.playerid}'
                        AND activ='1' """)
        await lcon.commit()
    return None

@dbopenconnect_decor
async def db_remPlayer(lcon, player: laddClasses.Player):
    lcurs = await lcon.execute(f""" SELECT * FROM players
                        WHERE playerid='{player.playerid}' 
                        AND activ='1' """)
    if len(await lcurs.fetchall())>0:
        await lcon.execute(f"""UPDATE players 
                            SET activ='0', teamid='0' , joindate=''
                            WHERE playerid='{player.playerid}'
                            AND activ='1' """)
        await lcon.commit()
    else:
        print("MSG: Could not find active player for removal.")
    return None


@dbopenconnect_decor
async def db_getPlayer(lcon, ID: int):
    lcurs = await lcon.execute(f"""SELECT * FROM players WHERE playerid='{ID}' """)
    fetch = await lcurs.fetchall()
    if len(fetch)>0:
        returnVal = laddClasses.Player(fetch[0])
    else:
        returnVal = None
    return returnVal


# ---------------------------------------------------
# Match functionalities
# ---------------------------------------------------
@dbopenconnect_decor
async def db_getActMatches(lcon):
    lcurs = await lcon.execute(f"""SELECT * FROM matches WHERE status<3 """)
    fetch = await lcurs.fetchall()
    if len(fetch)==0:
        retList = None
    else:
        retList = [laddClasses.Match(row) for row in fetch ]
    return retList


@dbopenconnect_decor
async def db_getMaxMatchID(lcon):
    lcurs = await lcon.execute(f"""SELECT * FROM matches""")
    fetch = await lcurs.fetchall()
    matchlist = [laddClasses.Match(match).matchid for match in fetch]
    if len(matchlist)==0:
        matchlist = [0]
    return max(matchlist)


@dbopenconnect_decor
async def db_getMatch(lcon, ID: int):
    lcurs = await lcon.execute(f"""SELECT * FROM matches WHERE matchid='{ID}' """)
    fetch = await lcurs.fetchall()
    if len(fetch)>0:
        match = laddClasses.Match(fetch[0])
    else:
        match = None
    return match


@dbopenconnect_decor
async def db_initMatch(lcon, team1id: int, team2id: int):
    if not (await db_getTeam(team1id) is None) and not (await db_getTeam(team2id) is None):
        newID = await db_getMaxMatchID() + 1
        match = laddClasses.Match(newID, team1id, team2id)
        [keys, values] = match.getQueryStr()
        await lcon.execute(f"""INSERT INTO matches {keys} VALUES {values}  """)
        await lcon.commit()
        return match
    else:
        print("MSG: Could not find the right team. Check team ID-s again.")
        return None


@dbopenconnect_decor
async def db_cancelMatch(lcon, matchid: int):
    match = await db_getMatch(matchid)
    if not match is None:

        if match.activ==0:
            print("MSG: Could not find match. (inactive)")
        elif match.status==3:
            print("MSG: Match is already finished.")
        else:
            await lcon.execute(f""" UPDATE matches SET activ='0'
                                WHERE matchid='{match.matchid}' """)
            await lcon.commit()
    else: 
        print("MSG: Could not find match.")
    return None


@dbopenconnect_decor
async def db_updateMatch(lcon, match: laddClasses.Match):
    checkMatch = await db_getMatch(match.matchid)
    if checkMatch is None:
        print("MSG: Could not find match.")
    else: 
        await lcon.execute(f""" UPDATE matches 
                            SET preelo1='{match.preelo1}', preelo2='{match.preelo2}',
                                score1='{match.score1}', score2='{match.score2}', 
                                eloc1='{match.eloc1}', eloc2='{match.eloc2}',
                                date='{match.date}',status='{match.status}',
                                map='{match.map}' 
                            WHERE matchid='{match.matchid}' """ )
        await lcon.commit()
    return None


@dbopenconnect_decor
async def db_updateSQL(lcon, table: str, value: str, cond: str):
    """ Utility command to change values in DB using bot. """
    lcurs = await lcon.execute(f""" UPDATE {table}
                                    SET {value}
                                    WHERE {cond} """ )
    await lcon.commit()
    return None


if __name__=="__main__": 
    pass

