import asyncio
import bot
import discord
import logging
from bot import utilities as botutils
from discord.ext import commands, tasks
from modules import laddConfig, laddClasses, syncDBHandler

logger = logging.getLogger('DCBOT')


class EventsCog(commands.Cog):
    """ Cog for events. """

    def __init__(self, bot):
        self.bot = bot
        self.name = "bot.events"

    def cog_unload(self):
        self.autoLadderTableUpdate.cancel()


    @commands.Cog.listener()
    async def on_ready(self):
        con = await syncDBHandler.dbinit_connection(laddConfig.dbname)
        try:
            await con.cursor()
            logger.info(f"DB has been connected.")
        except Exception as ex:
            logger.info("Error establishing connection to DB.")
            raise ValueError
        print(f"> Logged in as '{self.bot.user.name}' with id '{self.bot.user.id}'.")
        for guild in self.bot.guilds:
            print(f"> Connected to {guild.name} with id '{guild.id}'")
        await self.bot.change_presence(activity=discord.Activity(type=discord.ActivityType.watching,
                                        name="gitlab.com/mori_stellam/DiscordLadder"))
        print('> Ready for work.')
        self.autoLadderTableUpdate.start()
        await botutils.repostBotEmbeds(self.bot)
        return None


    @tasks.loop(seconds=21600)
    async def autoLadderTableUpdate(self):
        await botutils.updateLadder(chan=self.bot.get_guild(laddConfig.LadderInt).get_channel(laddConfig.LadderChannelID))
        return None

    
def setup(bot):
    bot.add_cog(EventsCog(bot))