import asyncio
import discord
from discord.ext import commands
from modules import laddClasses as classes

repoUrl = "https://gitlab.com/mori_stellam/DiscordLadder"


def getSingleEmbed(lbot: commands.Bot, header: str, msg: str):
    """ A single simple embed as slash command  feedback."""
    theEmbed = discord.Embed(title="Feedback", url=repoUrl, description=" ")
    theEmbed.set_author(name=lbot.user.name, url=repoUrl, icon_url=lbot.user.avatar.url)
    theEmbed.add_field(name=header, inline=False,value = msg)
    return theEmbed


def getInvitePlayerEmbed(lbot: commands.Bot, player: classes.Player, team: classes.Team):
    theEmbed = discord.Embed(title="Player Invitation", url=repoUrl, description=" ")
    theEmbed.set_author(name=lbot.user.name, url=repoUrl, icon_url=lbot.user.avatar.url)
    theEmbed.add_field(name=f"Invitation to '{team.name}'", inline=False,
                        value = f"<@{team.captain}> has invited <@{player.playerid}>" + \
                                f" to **{team.name}** {team.loc}. <@{player.playerid}>, do you " + \
                                "accept the invitation?")
    theEmbed.set_footer(text="Only the inviting and the invited players can respond.")
    return theEmbed


def getMatchEmbed(lbot: commands.Bot,
                    match: classes.Match,
                    cpt1: classes.Player, 
                    cpt2: classes.Player,
                    teamOne: classes.Team,
                    teamTwo: classes.Team,
                    mapBanList: list=[], 
                    currCpt: int=0, 
                    lastBan: str="",
                    reportMsg: str="", 
                    result: int=0, 
                    resolver: int=0):
    theEmbed = discord.Embed(title=f"Ladder Match : {teamOne.name}' {teamOne.loc} vs {teamTwo.name}' {teamTwo.loc} " + \
                            f"(ID: {match.matchid})",
                        url=repoUrl, description="Ranked match in team ladder.")
    theEmbed.set_author(name=lbot.user.name, url=repoUrl, icon_url=lbot.user.avatar.url)
    if match.status==1:
        theEmbed.add_field(name="Stage 1: Challenge invitation", inline=False,
                        value=f"Team '{teamOne.name}' {teamOne.loc} (<@{cpt1.playerid}>) has challenged " + \
                            f" Team '{teamTwo.name}' {teamTwo.loc} (<@{cpt2.playerid}>). " + \
                            f"Do you accept the challenge, <@{cpt2.playerid}>?")
        theEmbed.set_footer(text="Note: Both captains can cancel the invitation.")
    elif match.status==2:
        if lastBan=="":
            msgContent = f"<@{currCpt.playerid}>'s turn to ban a map. Current map pool: ```"
        else:
            msgContent = f"<@{currCpt.playerid}>'s turn to ban a map. Last map banned: **{lastBan}**. Current map pool: ```"
        for tmap in mapBanList:
            msgContent += f"{tmap}\n"
        msgContent += "```"
        theEmbed.add_field(name=f"Stage 2: Map ban", inline=False, value=msgContent)
        theEmbed.set_footer(text="Captains are taking turns to ban a map until there are 3 or less maps left and " + \
                                "until both captains banned the same amount of maps. Then the final map is " + \
                                "randomly chosen.")
    elif match.status==3:
        msgContent = f"\n Match between '{teamOne.name}' {teamOne.loc} and '{teamTwo.name}' {teamTwo.loc} " + \
                    f"(Match ID '{match.matchid}') on **{match.map}** is ongoing. \n" + \
                    f"Both captains have to report the result or the intention to cancel below.\n" + \
                    f"Please let admins know if there are issues or there is contradiction."
        if reportMsg!="":
            msgContent += (" \n " + reportMsg)
        theEmbed.add_field(name=f"Stage 3: Match result", inline=False, value=msgContent)
        theEmbed.set_footer(text="Match is resolved when both captains push the same button or when an admin " + \
                                "reports the match result.")
    elif match.status==4:
        msgContent = f"\n Match between '{teamOne.name}' {teamOne.loc} and '{teamTwo.name}' {teamTwo.loc} " + \
                    f"(Match ID '{match.matchid}') on **{match.map}** is concluded. \n"
        if resolver!=0:
            msgContent += "_Admin resolved conflict._ "
        if result==1:
            msgContent += f"\n Winner is **{teamOne.name}**."
        elif result==2:
            msgContent += f"\n **Match is a draw.**"
        elif result==3:
            msgContent += f"\n Winner is **{teamTwo.name}**."
        else:
            msgContent += "Unknown match result."
        theEmbed.add_field(name="Stage 4: Match ended.", inline=False, value=msgContent)
        theEmbed.set_footer(text="Team rankings updated.")

    elif match.status==101:
        theEmbed.add_field(name="Outcome 101: Rejected challenge", inline=False,
                        value=f"<@{cpt2.playerid}> has rejected the challenge. Match is cancelled.")
    elif match.status==102:
        theEmbed.add_field(name="Outcome 102: Match cancelled", inline=False,
                        valuef=f"Match (ID '{match.matchid}') between {teamOne.name} and {teamTwo.name} is cancelled.")
        theEmbed.set_footer(text="Both captains reported match cancellation.")
    else:
        theEmbed.add_field(name="ERROR", inline=False, value="Unknown match state.")
        theEmbed.set_footer(text="Ask bot maintainer to check console.")
    return theEmbed

