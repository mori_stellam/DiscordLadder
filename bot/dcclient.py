from discord import Intents
from discord.ext import commands

dcint = Intents.default()
dcint.dm_messages = True
dcint.dm_typing = False
dcint.dm_reactions = True
dcint.emojis = True
dcint.guilds = True
dcint.guild_messages = True
dcint.guild_reactions = True
dcint.guild_typing = False
dcint.members = True
dcint.presences = False
dcint.typing = False
dcint.reactions = True

dcbot = commands.Bot(command_prefix="/", self_bot=True, intents=dcint)