import asyncio
from dis import dis
import logging
import discord
from bot import embeds as boeds
from bot import utilities as botutils
from discord.commands import Option, slash_command
from discord.ext import commands
from modules import laddConfig as config
from modules import laddClasses as classes
from modules import syncDBHandler


logger = logging.getLogger('discord')
timesleep = config.TimeSleep

async def deletemsg(themsg):
    try:
        await themsg.delete()
    except:
        await themsg.delete_original_message()
    return None

class CommandsCog(commands.Cog):
    """ Cogs for commands from discord. """

    def __init__(self, abot):
        self.bot = abot
        self.name = "bot.commands"


    # ------------------------------------------------------
    # Utility commands for initializing and updating channels
    # ------------------------------------------------------
    @slash_command(guild_ids=[config.LadderInt])
    async def setadminrole(self, ctx: discord.ApplicationContext,
                    therole: Option(discord.Role, "Select a role.")):
        """ Initializes admin role. """
        logger.info(" >>> Request to initialize or change admin role.")
        if config.AdminRole<0 and ctx.guild.id==config.LadderInt:
            config.AdminRole = therole.id
            config.parsedParams["DCCONFIG"]["adminRole"] = str(therole.id)
            with open('config.ini', 'w') as cfgfile:
                config.parsedParams.write(cfgfile)
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "Admin role has been assigned."))
        elif config.AdminRole>0 and ctx.guild.id==config.LadderInt:
            if config.AdminRole in [role.id for role in ctx.author.roles]:
                config.AdminRole = therole.id
                config.parsedParams["DCCONFIG"]["adminRole"] = str(therole.id)
                with open('config.ini', 'w') as cfgfile:
                    config.parsedParams.write(cfgfile)
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "Admin role has been assigned."))
            else:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "Admin role could not be changed as you are not an admin."))
        else:
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "Wrong server."))

        await asyncio.sleep(timesleep)
        await deletemsg(theMsg)


    @slash_command(guild_ids=[config.LadderInt])
    async def initladdertable(self, ctx: discord.ApplicationContext):
        """ Initialize Leaderboard table in a text channel
            - Saves channel ID
            - Clears the channel
            - Sends a message showing the table
        """
        logger.info(" >>> Request to initialize channel to display ladder.")
        if botutils.checkUserAdmin(ctx.author):
            if config.LadderChannelID<0:
                config.LadderInt = ctx.guild.id
                config.LadderChannelID = ctx.channel.id
                config.parsedParams["DCCONFIG"]["LadderInt"] = str(ctx.guild.id)
                config.parsedParams["DCCONFIG"]["LadderChannelID"] = str(ctx.channel.id)
                with open('config.ini', 'w') as cfgfile:
                    config.parsedParams.write(cfgfile)
                await botutils.updateladder(ctx)

                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback","Table initialized"))
                logger.info(" >>> Table has been initialized")
                logger.info(" >>> Might need to restart bot after initialization.")
                await asyncio.sleep(timesleep)
                await deletemsg(theMsg)
            else:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "Table has been initalized (maybe somewhere else)."))
                logger.info(" >>> Table has been initalized.")
                await asyncio.sleep(timesleep)
                await deletemsg(theMsg)
        else:
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "You are not authorized."))
            await asyncio.sleep(timesleep)
            await deletemsg(theMsg)


    @slash_command(guild_ids=[config.LadderInt])
    async def initteamroster(self, ctx: discord.ApplicationContext):
        """ Initializes channel to display team rosters. Similar to `initladdertable` above."""
        logger.info(" >>> Request to initialize channel to display team rosters.")
        if botutils.checkUserAdmin(ctx.author):
            if config.TeamChannelID != ctx.channel.id:
                # Save channel and update config file
                config.TeamChannelID = ctx.channel.id
                config.parsedParams["DCCONFIG"]["TeamChannelID"] = str(ctx.channel.id)
                with open('config.ini', 'w') as cfgfile:
                    config.parsedParams.write(cfgfile)

                # Clear channel, post ladder and finish
                await botutils.updateTeamRosters(ctx, self.bot)
                await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "Team rosters initialized."))
                logger.info(" >>> Team rosters initialized.")

            else:
                await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "Table has been initalized (maybe somewhere else)."))
                logger.info(" >>> Table has been initalized.")
        else:
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "You are not authorized."))
            await asyncio.sleep(timesleep)
            await deletemsg(theMsg)
        return None


    @slash_command(guild_ids=[config.LadderInt])
    async def initmatcheschannel(self, ctx: discord.ApplicationContext):
        """ Initialize text channel for matches. """
        logger.info(" >>> Request to initialize channel for matches. ")
        if config.MatchesChannelID<0 and ctx.channel.id!=config.LadderChannelID and \
                    ctx.channel.id!=config.TeamChannelID:
            config.MatchesChannelID = ctx.channel.id
            config.parsedParams["DCCONFIG"]["MatchesChanellID"] = str(ctx.channel.id)
            with open('config.ini', 'w') as cfgfile:
                config.parsedParams.write(cfgfile)

             # Clearn channel and post ladder
            await botutils.updateLadder(ctx)
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "Channel saved for matches."))
            logger.info(">>> Channel initialized for matches.")
            logger.info(" >>> Might need to restart bot after initialization.")
        else:
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "Table has been initalized (maybe somewhere else)."))
            logger.info(" >>> Table has been initalized.")
        await asyncio.sleep(timesleep)
        await deletemsg(theMsg)


    @slash_command(guild_ids=[config.LadderInt])
    async def updateladdertable(self, ctx: discord.ApplicationContext):
        """ Utility command to re-post ladder table. """
        logger.info(" >>> Request to update ladder table.")
        if botutils.checkUserAdmin(ctx.author):
            if config.LadderChannelID==ctx.channel.id:
                await ctx.defer()
                await botutils.updateLadder(ctx)
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "Ladder table updated."))
            else:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "Wrong channel or channel has not been initalized."))
        else:
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "You are not an admin."))
        await asyncio.sleep(timesleep)
        await deletemsg(theMsg)
        return None


    @slash_command(guild_ids=[config.LadderInt])
    async def updateteamrosters(self, ctx: discord.ApplicationContext):
        """ Utility command to re-post team rosters."""
        logging.info(">>> Requested update of team rosters.")
        if botutils.checkUserAdmin(ctx.author):
            if config.TeamChannelID == ctx.channel.id:
                await ctx.defer()
                await botutils.updateTeamRosters(ctx, self.bot)
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "Team rosters updated."))
                logging.info(" >>> Team rosters updated.")
            else:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "Wrong channel or channel has not been initalized."))
        else:
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "You are not authorized."))
        await asyncio.sleep(timesleep)
        await deletemsg(theMsg)


    @slash_command(guild_ids=[config.LadderInt])
    async def updatesql(self, ctx: discord.ApplicationContext,
                        tablename: Option(str, "Name of SQL table"),
                        value: Option(str, "Key-values to set"),
                        condition: Option(str, "Condition")):
        """ Command to manually update values in SQL. """
        logger.info(" >>> Request to update ladder table.")
        if botutils.checkUserAdmin(ctx.author):
            await syncDBHandler.db_updateSQL(tablename, value, condition)
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "SQL row(s) updated."))
        else:
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "You are not authorized."))
        await asyncio.sleep(timesleep)
        await deletemsg(theMsg)

    
    @slash_command(guild_ids=[config.LadderInt])
    async def repostembeds(self, ctx: discord.ApplicationContext):
        """ Reposts active embeds """
        await ctx.respond(content="Doing it.", ephemeral=True)
        await botutils.repostBotEmbeds(self.bot)
        return None


    # --------------------------
    # Team management commands
    # --------------------------
    @slash_command(guild_ids=[config.LadderInt])
    async def createteam(self, ctx: discord.ApplicationContext, 
                        teamname: Option(str, "Team name"),
                        regionflag: Option(str, "Region flag of the team. E.g. :flag_eu:")):
        await ctx.defer()
        resp = await botutils.addTeam(ctx, self.bot, teamname, regionflag)
        if resp==1:
            theMsg = await ctx.respond(f" User {ctx.author.name} with ID {ctx.author.id} is already in a team.")
            logger.info(f" User {ctx.author.name} with ID {ctx.author.id} is already in a team.")
        elif resp==2:
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", f" Team name {teamname} is already taken."))
            logger.info(f"Team name {teamname} is already taken.")
        elif resp==0:
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback",
                                f" New team named '{teamname}' has been added. " + \
                                "Need at least 5 players to appear on the leaderboard."))
        await asyncio.sleep(timesleep)
        await deletemsg(theMsg)
        return None


    @slash_command(guild_ids=[config.LadderInt])
    async def disbandteam(self, ctx: discord.ApplicationContext):
        """ Command to disband team. """
        logger.info(">>> Received request to disband team.")
        if ctx.channel.id==config.TeamChannelID:
            await ctx.defer()
            resp = await botutils.remTeam(ctx, self.bot)
            if resp==1:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "You are not captain of a team."))
            else:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", f"Team '{resp[1]}' (ID {resp[0]}) has been disbanded."))
        else:
            theMsg = await ctx.respond(f"Wrong channel.", ephemeral=True)
        await asyncio.sleep(timesleep)
        await deletemsg(theMsg)


    @slash_command(guild_ids=[config.LadderInt])
    async def addplayertoteam(self, ctx: discord.ApplicationContext,
                    player: Option(discord.User, "Discord user to be added to the team"),
                    teamid: Option(int, "ID of the team.")):
        """ Utility commad to add a player to a team. """
        if ctx.channel.id==config.TeamChannelID:
            if botutils.checkUserAdmin(ctx.author):
                resp = await botutils.addPlayerToTeam(ctx, player, teamid)
                if resp==1:
                    theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback",
                                        f"Player '{player.name}' is part of another team or already invited to another team."))
                elif resp==2:
                    theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", f"Cannot find a team with ID '{teamid}'"))
                elif resp==3:
                    theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", f"Team is already full or has invited enough people."))
                elif isinstance(resp, str):
                    theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", f"Player '{player.name}' is added to team '{resp}'."))
                    await botutils.updateLadder(chan=self.bot.get_guild(config.LadderInt).get_channel(config.LadderChannelID))
                else:
                    theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", f"Unknown error."))
            else:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "You are not authorized."))
        else:
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "Command is issued in the wrong channel."))
        await asyncio.sleep(timesleep)
        await deletemsg(theMsg)
        return None


    @slash_command(guild_ids=[config.LadderInt])
    async def inviteplayer(self, ctx: discord.ApplicationContext, 
                    player: Option(discord.User, "Discord user to be invited to your team.")):
        """ User command to invite a person to a team. Should be permission tied. """
        logger.info(">>> Received request to invite a player.")
        if ctx.channel.id==config.TeamChannelID:
            await ctx.defer()
            resp = await botutils.playerInvitation(self.bot, ctx, player)
            if resp==1 or resp==2:
                theMsg = await ctx.respond( embed=boeds.getSingleEmbed(self.bot, "Feedback",
                                        f"Error. Inviting player ({ctx.author.name}) is not a team captain."))
            elif resp==3:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback",
                                        f"Error. Invited player ({player.name}) is already in a team."))
            elif resp==4:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback",
                                        f"Error. Team is already full (including invited players)."))
            elif resp==-1:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback",
                                        f"<@{player.id}> has rejected the invitation or <@{ctx.author.id}> has canceled."))
            elif resp==0:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback",
                                        f"<@{player.id}> has accepted the invitation"))
        else:
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", f"Unknown error."))
        await asyncio.sleep(timesleep)
        await deletemsg(theMsg)
        return None


    @slash_command(guild_ids=[config.LadderInt])
    async def kickplayer(self, ctx: discord.ApplicationContext,
                        player: Option(discord.User, "Discord user to be kicked from the team.")):
        """ Team captain command to kick a player who is not a captain. """
        logger.info(">>> Received request to kick a player.")
        if ctx.channel.id==config.TeamChannelID:
            resp = await botutils.kickPlayer(ctx, player)
            if resp==1:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback",
                                        "You cannot kick yourself. If you are a captain, change captain or disban the whole team."))
            elif resp==2:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback",
                                        f"<@{player.id}> is not in the team."))
            elif resp==3:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", f"<@{ctx.author.id}> is not a team captain."))
            elif resp==4:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback",
                                f"<@{player.id}> has joined within {config.defTFixDays} " + \
                                "days and cannot be removed from the team yet." ))
            else:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", f"<@{player.id}> has been removed from '{resp.name}''."))
        else:
            theMsg = await ctx.respond(f"Wrong channel.")
        await asyncio.sleep(timesleep)
        await deletemsg(theMsg)
        return None


    @slash_command(guild_ids=[config.LadderInt])
    async def leaveteam(self, ctx: discord.ApplicationContext):
        """ User command to leave one's team. """
        logger.info(f">>> '{ctx.author.name}' issued command to leave team.")
        if ctx.channel.id==config.TeamChannelID:
            resp = await botutils.leaveTeam(ctx)
            if resp==1:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "Error. You are not in a team."))
            elif resp==2:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "Error. Cannot leave the team yet. You need to wait {config.defTFixDays} days after joining."))
            elif resp==3:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "Error. Cannot leave as captain. Pass on the captain role before leaving or disband team."))
            elif resp==0:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "You have been removed from the team."))
                await botutils.updateTeamRosters(ctx, self.bot)
        else:
            theMsg = await ctx.respond(f"Wrong channel.")
        await asyncio.sleep(timesleep)
        await deletemsg(theMsg)


    @slash_command(guild_ids=[config.LadderInt])
    async def changecaptain(self, ctx: discord.ApplicationContext,
                        newcpt: Option(discord.User, "Discord user from team to be assigned as team captain.")):
        """ Assign new captain for the team. """
        logger.info(">>> Request to assign new captain by '{ctx.author.name}'.")
        if ctx.channel.id==config.TeamChannelID:
            resp = await botutils.changeCaptain(ctx, newcpt)
            if resp==1:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", f"Error. You are not a captain of a team."))
            elif resp==2:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", f"Error. '{newcpt.name}' is not in a team."))
            elif resp==3:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", f"Error. '{newcpt.name}' is not in the same team."))
            else:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", f"Captain has been assigned to <@{newcpt.id}>."))
                await botutils.updateTeamRosters(ctx, self.bot)
        else:
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", f"Wrong channel."))
        await asyncio.sleep(timesleep)
        await deletemsg(theMsg)


    # ------------------------------------------
    # Match related commands
    # ------------------------------------------
    @slash_command(guild_ids=[config.LadderInt])
    async def challenge(self, ctx: discord.ApplicationContext,
                    challengedteamid: Option(int, "ID of team to be challenged.")):
        """ Command to challenge another team for a match. """
        if ctx.channel.id==config.MatchesChannelID:
            await ctx.defer()
            resp = await botutils.challengeTeam(self.bot, ctx, challengedteamid)
            if resp==1:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "You are not the captain of a team."))
            elif resp==2:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "Cannot find team with specified ID."))
            elif resp==3:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback",
                                "One of the teams is not challengeable. Potential reasons: \n" + \
                                "- Not enough eligible players in team (min: 5). \n" + \
                                f"- One of the team is in too many unfinished matches (max: {config.defMaxMatches}) \n" + \
                                f"- Challenged team is out of challengeable range (+- {config.ChallengeRankRange} ranks)."))
            elif resp==0:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback","Challenge initiated."))
                #updateScoreschannel = self.bot.get_guild(config.LadderInt).get_channel(config.LadderChannelID)
                #await botutils.updateLadder(chan=updateScoreschannel)
            else:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback","Challenge initiated."))
        else:
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "Wrong channel."))
        await asyncio.sleep(timesleep)
        await deletemsg(theMsg)


    # ------------------------------------------
    # Utility commands
    # ------------------------------------------
    @slash_command(guild_ids=[config.LadderInt])
    async def updatesql(self, ctx: discord.ApplicationContext,
                    tablename: Option(str, "Table name"), value: Option(str, "Values to be set"), condition: Option(str, "Condition")):
        """ Command to directly update the SQLite datbaase. """
        logger.info(" >>> Request to update ladder table.")
        if botutils.checkUserAdmin(ctx.author):
            await syncDBHandler.db_updateSQL(tablename, value, condition)
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "SQL row(s) updated."))
        else:
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Feedback", "You are not authorized."))

        await asyncio.sleep(timesleep)
        await deletemsg(theMsg)




def setup(bot):
    bot.add_cog(CommandsCog(bot))
