# ######################################################################################
# This module is responsible for all the back-end processes and where discord interface
# interacts with the database.
# ######################################################################################


import asyncio
import discord
import tabulate
import trueskill
import random
import re
from bot import embeds as boeds
from datetime import datetime
from discord import utils as dcutils
from discord import TextChannel, Member
from discord.ext import commands
from modules import syncDBHandler, laddClasses, laddConfig
from itertools import chain


RATELIMITWAIT = 0.8
GL_TeamChoiceList = []
match_rep_cid_green = "match_rep_green"
match_rep_cid_gray = "match_rep_gray"
match_rep_cid_blue = "match_rep_blue"
match_rep_cid_red = "match_rep_red"

# --------------------------------------------------------------------------------------
# Supporting functions
# --------------------------------------------------------------------------------------
def checkUserAdmin(theuser: Member):
    """ Check if member is an admin/mod. """
    flag = False
    if laddConfig.AdminRole<0:
        flag = True
    else:
        if laddConfig.AdminRole>0 and (laddConfig.AdminRole in [p.id for p in theuser.roles]):
            flag = True
    return flag


def getReportedResultLbl(team1: laddClasses.Team, team2: laddClasses.Team, choice: str):
    if choice==match_rep_cid_green:
        return f"{team1.name} wins"
    elif choice==match_rep_cid_blue:
        return f"{team2.name} wins"
    elif choice==match_rep_cid_gray:
        return "Draw."
    else:
        return "Unknown outcome."


async def getLadderChunks():
    """ Query teams from database and convert them to bot messages. """
    rawTeams = await syncDBHandler.queryLeaderboard()
    if len(rawTeams) > 0:
        Teams = []
        thisDay = datetime.today()
        for team in rawTeams:
            roster = await syncDBHandler.db_getTeamRoster(team)
            counter = 0
            for player in roster:
                playerJoined = datetime.strptime(player.joindate, "%d-%m-%y")
                delta = thisDay - playerJoined
                if delta.days>=laddConfig.defTFixDays:
                    counter +=1
            if counter>=5:
                Teams.append(team)
        if  len(Teams)>0:
            def convertTeamToList(team: laddClasses.Team):
                return [team.teamid, team.name, team.win, team.draw, team.loss, int(team.elo)]
            teamdatalist = [convertTeamToList(team) for team in Teams]
            headers = ["Team ID", "Name", "Wins", "Draws", "Losses", "Score"]

            def genChunkList(thelist, n):
                for i in range(0, len(thelist), n):
                    ranks = range(i+1, min(i+n+1, (len(thelist) % n)+1))
                    yield tabulate.tabulate(thelist[i: i+n], headers=headers, tablefmt="presto", showindex=ranks)
            return [item for item in genChunkList(teamdatalist, laddConfig.LadderTeamPerMsg)]
        else:
            return ["    |   Team ID | Name        |   Wins |   Draws |   Losses |   Score "]
    else:
        return ["    |   Team ID | Name        |   Wins |   Draws |   Losses |   Score "]


async def getTeamListForChoices():
    """ Return list of teams """
    Teams = await syncDBHandler.queryLeaderboard()
    if len(Teams)>0:
        choices = []
        for team in Teams:
            choices.append(discord.SelectOption(label=team.name, value=team.teamid))
    else:
        choices(discord.SelectOption(label="There are no teams yet", value=-1))
    global GL_TeamChoiceList
    GL_TeamChoiceList = choices
    return choices


async def getTeamRosterChunks(ctx: discord.ApplicationContext):
    """ Query teams rosters from database and post team-by-team rosters. """
    Teams = await syncDBHandler.queryLeaderboard()
    if len(Teams)>0:
        rosterChunks = []
        theGuild = ctx.guild
        for team in Teams:
            # Create message per team
            roster = await syncDBHandler.db_getTeamRoster(team)
            cpt = roster[0]
            rosterString = ""
            for player in roster[1:]:
                # Construct roster part
                dcPlayer = theGuild.get_member(player.playerid)
                if dcPlayer is not None:
                    if rosterString=="":
                        rosterString = f"{dcPlayer.display_name} "
                    else:
                        rosterString = rosterString + f"\n {dcPlayer.display_name}"
                else:
                    pass
            # Add header to the roster part
            MessageChunk = f""" **{team.name}** (ID: {team.teamid}) """ + \
                           f""" {team.loc} - Captain: <@{cpt.playerid}>  """ + \
                           f""" ``` {rosterString} ```"""
            rosterChunks.append(MessageChunk)
    else:
        rosterChunks = []
    return rosterChunks


async def getTeamRankLeaderboard(Team: laddClasses.Team):
    """ Returns team ranking on the leaderboard. """
    Teams = await syncDBHandler.queryLeaderboard()
    TeamIDs = [team.teamid for team in Teams]
    try:
        retVal = TeamIDs.index(Team.teamid)
    except:
        retVal = None
    return retVal


async def checkChallengeableTeams(teamOne: laddClasses.Team, teamTwo: laddClasses.Team):
    """ Check conditions if teams are challengeable."""
    flag = True
    # Count eligible players
    rosterOne = await syncDBHandler.db_getTeamRoster(teamOne)
    rosterTwo = await syncDBHandler.db_getTeamRoster(teamTwo)
    today = datetime.today()
    for roster in [rosterOne, rosterTwo]:
        count = 0
        for player in roster:
            jdate = datetime.strptime(player.joindate, "%d-%m-%y")
            delta = today - jdate
            count = count + (delta.days>=laddConfig.defTFixDays)
        if count<5:
            flag = False

    # Count number of matches teams are involved
    activMatches = await syncDBHandler.db_getActMatches()
    if not (activMatches is None):
        matchteams2d = [[match.team1, match.team2] for match in activMatches]
        matchteams = list(chain.from_iterable(matchteams2d))
        c1 = sum([i==teamOne.teamid for i in matchteams])
        c2 = sum([i==teamTwo.teamid for i in matchteams])
        if c1>=laddConfig.defMaxMatches | c2>=laddConfig.defMaxMatches:
            flag = False

    # Check if rank differences are large or not
    rankTOne = await getTeamRankLeaderboard(teamOne)
    rankTTwo = await getTeamRankLeaderboard(teamTwo)
    if (rankTOne is not None) and (rankTTwo is not None):
        if abs(rankTOne-rankTTwo) > laddConfig.ChallengeRankRange:
            flag = False
    else:
        flag = False
    return flag


# --------------------------------------------------------------------------------------
# REPOST function
# --------------------------------------------------------------------------------------
async def repostBotEmbeds(lbot: commands.Bot):
    """ Repost player invitations and match statuses. """
    channel = await lbot.get_guild(laddConfig.LadderInt).fetch_channel(laddConfig.TeamChannelID)
    async for message in channel.history(limit=2000, oldest_first=True):
        if len(message.embeds)>0:
            thembed = message.embeds[0]
            if thembed.author.name == lbot.user.name and thembed.title == "Player Invitation" and \
                len(message.components)>0:
                field = thembed.fields[0]
                teamname = re.search("Invitation to '(.*)'", field.name).group(1)
                plrid = re.search("<@[0-9]*> has invited <@([0-9]*)>", field.value).group(1)
                team = await syncDBHandler.db_getTeamByName(teamname)
                cpt = await syncDBHandler.db_getPlayer(team.captain)
                player = await syncDBHandler.db_getPlayer(plrid)
                await message.delete()
                theView = PlayerInviteView(lbot, player, cpt, team)
                theEmbed = boeds.getInvitePlayerEmbed(lbot, player, team)
                await channel.send(embed=theEmbed, view=theView)
    
    channel2 = await lbot.get_guild(laddConfig.LadderInt).fetch_channel(laddConfig.MatchesChannelID)
    async for message in channel2.history(limit=2000, oldest_first=True):
        if len(message.embeds)>0:
            thembed = message.embeds[0]
            if thembed.author.name == lbot.user.name and ("Ladder Match" in thembed.title) and \
                len(message.components)>0:
                field = thembed.fields[0]
                team1name = re.search(" : (.*)' .* vs", thembed.title).group(1)
                team2name = re.search(" vs (.*)' .* \(ID", thembed.title).group(1)
                matchid = re.search("\(ID: ([0-9]*)\)", thembed.title).group(1)
                match = await syncDBHandler.db_getMatch(matchid)
                team1 = await syncDBHandler.db_getTeamByName(team1name)
                team2 = await syncDBHandler.db_getTeamByName(team2name)
                cpt1 = await syncDBHandler.db_getPlayer(team1.captain)
                cpt2 = await syncDBHandler.db_getPlayer(team2.captain)
                maplist=[]
                lastban = ""
                currBan = cpt2
                if match.status==1:
                   theView = MatchViewST1(lbot, cpt1, cpt2, team1, team2, match)
                elif match.status==2: 
                    if field.name=="Stage 2: Map ban":
                        maptxt = re.search("```(.*)```", field.value).group(1)
                        maplist = maptxt.strip().split("\n")
                        lastban = re.search("Last map banned: **(.*)**.", field.value).group(1)
                        currBan = await syncDBHandler.db_getPlayer(
                                    re.search("(.*)'s turn to ban a map.", field.value).group(1)
                        )
                    else:
                        maplist = [tmap for tmap in laddConfig.MapList]
                    theView = MatchViewST2(lbot, cpt1, cpt2, team1, team2, match, maplist, lastban,currBan)
                elif match.status==3:
                    theView = MatchViewST3(lbot, cpt1, cpt2, team1, team2, match)
                else:
                    theView = None
                if match.status<4:
                    theEmbed = boeds.getMatchEmbed(lbot, match, cpt1, cpt2, team1, team2, maplist, 
                                                currBan.playerid, lastban)
                    await message.delete()
                    await channel2.send(embed=theEmbed, view=theView)
    return None


# --------------------------------------------------------------------------------------
# Utility command functions
# --------------------------------------------------------------------------------------
async def updateLadder(ctx: discord.ApplicationContext=None, chan: TextChannel=None):
    """ Clears all messages and re-posts team ladder. """
    if ctx is not None:
        theChannel = ctx.channel
    elif chan is not None:
        theChannel = chan
    else: # Either ctx or text channel is needed
        pass
    if laddConfig.LadderChannelID == theChannel.id:
        # Delete messages
        async for message in theChannel.history(limit=2000):
            checkContent = re.findall("    |   Team ID | Name        |   Wins |   Draws |   Losses |   Score ", message.content)
            if len(checkContent)>0:
                await message.delete()
                await asyncio.sleep(RATELIMITWAIT)

        # Send table
        messsageChunks = await getLadderChunks()
        for textTeam in messsageChunks:
            await theChannel.send("```" + textTeam + " ```")
            await asyncio.sleep(RATELIMITWAIT)
    else:
        pass
    return None


async def updateTeamRosters(ctx: discord.ApplicationContext, localbot: commands.Bot):
    """ Clears all messages and re-posts team rosters. """
    # Delete messages
    async for message in ctx.channel.history(limit=2000):
        checkContent = re.findall(".*\s\(ID: [0-9]*\)\s+.*\s+\-\sCaptain:\s.*", message.content)
        if message.author.id==localbot.user.id and len(checkContent)>0:
            await message.delete()
            await asyncio.sleep(RATELIMITWAIT)

    # Send table
    messsageChunks = await getTeamRosterChunks(ctx)
    if len(messsageChunks)>0:
        for textTeam in messsageChunks:
            await ctx.channel.send(textTeam)
            await asyncio.sleep(RATELIMITWAIT)
    return None


# --------------------------------------------------------------------------------------
# Team management command functions
# --------------------------------------------------------------------------------------
async def addTeam(ctx: discord.ApplicationContext, lbot: commands.Bot, tname: str, regionflag: str):
    flag = True
    # Check user issuing the command: if in db yet or whether member in another team.
    cptid = ctx.author.id
    cpt = await syncDBHandler.db_getPlayer(cptid)
    if cpt is None:
        cpt = laddClasses.Player(cptid, ctx.author.name, 0, "", laddConfig.defPElo, 1)
        await syncDBHandler.db_addPlayer(cpt)
    if cpt.teamid != 0:
        # Player already in another team
        flag = False
        return 1

    # Check whether team name is taken
    theTeam = laddClasses.Team(-1, tname, cptid, 0, 0, 0, 0, laddConfig.defTElo, regionflag, 1)
    tNameTaken = await syncDBHandler.db_checkTeamName(tname)
    if tNameTaken:
        flag = False
        return 2

    # Add new team
    if flag == True:
        newID = random.randrange(10000, 99999)
        checkTeam = await syncDBHandler.db_getTeam(newID)
        while checkTeam is not None:
            newID = random.randrange(10000, 99999)
            checkTeam = await syncDBHandler.db_getTeam(newID)
        theTeam.teamid = newID
        await syncDBHandler.db_addTeam(theTeam)
        await syncDBHandler.db_addPlayerToTeam(cpt, theTeam)
        await updateTeamRosters(ctx, lbot)
        return 0


async def remTeam(ctx: discord.ApplicationContext, lbot: commands.Bot):
    """ Function to handle removing a team. """
    flag = True
    cptid = ctx.author.id
    cpt = await syncDBHandler.db_getPlayer(cptid)
    if cpt is None:
        flag = False
        return 1
    else:
        isTeam = await syncDBHandler.db_checkPlayerCaptain(cpt)
        if not isTeam:
            flag = False
            return 1
    if flag:
        theTeam = await syncDBHandler.db_getTeam(cpt.teamid)
        theRoster = await syncDBHandler.db_getTeamRoster(theTeam)
        for player in theRoster:
            player.teamid=0
            player.joindate=''
            await syncDBHandler.db_updatePlayer(player)
        theTeam.captain = 0
        theTeam.activ = 0
        await syncDBHandler.db_updateTeam(theTeam)
        await updateTeamRosters(ctx, lbot)
        return [theTeam.teamid, theTeam.name]


async def addPlayerToTeam(ctx: discord.ApplicationContext, player: commands.Bot.user, teamid: int):
    flag = True
    # Check if player is in DB
    dbPlayer = await syncDBHandler.db_getPlayer(player.id)
    if dbPlayer is None:
        dbPlayer = laddClasses.Player(player.id, player.name, 0, '', laddConfig.defPElo, 1)
        await syncDBHandler.db_addPlayer(dbPlayer)

    # Check if player is in a team already or if team is full dont add
    if dbPlayer.teamid != 0:
        flag = False
        return 1
    theTeam = await syncDBHandler.db_getTeam(teamid)
    if theTeam is None:
        flag = False
        return 2
    else:
        teamRoster = await syncDBHandler.db_getTeamRoster(theTeam)
        if len(teamRoster) >= laddConfig.defTMaxPlay:
            flag = False
            return 3

    # Add player if all conditions hold
    if flag:
        await syncDBHandler.db_addPlayerToTeam(dbPlayer, theTeam)
        return theTeam.name


async def playerInvitation(localBOT: commands.Bot, ctx: discord.ApplicationContext, player: commands.Bot.user):
    flag = True
    # Check inviting and invited user and the team in the datasasee
    dbPlayer = await syncDBHandler.db_getPlayer(player.id)
    dbInviting = await syncDBHandler.db_getPlayer(ctx.author.id)
    if dbPlayer is None:
        dbPlayer = laddClasses.Player(player.id, player.name)
        await syncDBHandler.db_addPlayer(dbPlayer)
    if dbInviting is None:      # Inviting player is not a captain
        flag = False
        dbInviting = laddClasses.Player(ctx.author.id, ctx.author.name)
        await syncDBHandler.db_addPlayer(dbInviting)
        return 1
    isCpt = await syncDBHandler.db_checkPlayerCaptain(dbInviting)
    if isCpt:                   # Inviting player is not a captain
        theTeam = await syncDBHandler.db_getTeam(dbInviting.teamid)
    else:
        flag = False
        return 2
    if dbPlayer.teamid != 0:      # Already in another team
        flag = False
        return 3
    if flag:                    # Team already full
        theRoster = await syncDBHandler.db_getTeamRoster(theTeam)
        if len(theRoster) >= laddConfig.defTMaxPlay:
            flag = False
            return 4

    # Send invitation
    if flag:
        dbPlayer.teamid = theTeam.teamid
        await syncDBHandler.db_updatePlayer(dbPlayer)
        # Generate message and components
        theEmbed = boeds.getInvitePlayerEmbed(localBOT, dbPlayer, theTeam)
        theView = PlayerInviteView(localBOT, dbPlayer, dbInviting, theTeam)
        theMsg = await ctx.send(embed=theEmbed, view=theView)
        # return [theMsg, theView]
        return 0


async def kickPlayer(ctx: discord.ApplicationContext, player: commands.Bot.user):
    """ A function that removes a player from the team per req. of captain."""
    # Check the identity of the invoker and the player in question
    flag = True
    if ctx.author.id==player.id:    # Cannot remove self
        flag = False
        return 1
    dbPlayer = await syncDBHandler.db_getPlayer(player.id)
    if dbPlayer is None:            # Player is not in a team
        flag = False
        return 2
    dbCpt = await syncDBHandler.db_getPlayer(ctx.author.id)
    if  dbCpt is None:              # Player is not captain
        flag = False
        return 3
    else:
        isCpt = await syncDBHandler.db_checkPlayerCaptain(dbCpt)
        if not isCpt:               # Player is not captain either
            flag = False
            return 3
        else:
            theTeam = await syncDBHandler.db_getTeam(dbCpt.teamid)
    if flag and dbPlayer.teamid!=theTeam.teamid:
        flag = False            # Player is not in the team.
        return 2
    if dbPlayer.joindate!="":   # Check whether it is not too son after joining the team
        thisDay = datetime.today()
        playerJoined = datetime.strptime(dbPlayer.joindate, "%d-%m-%y")
        delta = thisDay - playerJoined
        if delta.days<laddConfig.defTFixDays:
            flag = False
            return 4

    # Conditions hold, remove player
    if flag:
        await syncDBHandler.db_remPlayerFromTeam(dbPlayer)
        return theTeam


async def leaveTeam(ctx: discord.ApplicationContext):
    """ Function to allow players to leave their team. """
    # Check identity of invoker
    flag = True
    dbPlayer = await syncDBHandler.db_getPlayer(ctx.author.id)
    if dbPlayer is None:    # Player not in database yet
        flag = False
        return 1
    else:
        if dbPlayer.teamid==0:  # Player not in a team yet.
            flag = False
            return 1
        else:
            thisDay = datetime.today()
            delta = thisDay - datetime.strptime(dbPlayer.joindate, "%d-%m-%y")
            if delta.days < laddConfig.defTFixDays:
                flag = False
                return 2
        isCpt = await syncDBHandler.db_checkPlayerCaptain(dbPlayer)
        if isCpt and flag:
            flag = False
            return 3

    # Remove from team
    if flag:
        dbPlayer.teamid=0
        dbPlayer.joindate=''
        await syncDBHandler.db_updatePlayer(dbPlayer)
        return 0


async def changeCaptain(ctx: discord.ApplicationContext, player: commands.Bot.user):
    """ Pass captain role to another player """
    flag = True
    isCpt = await syncDBHandler.db_checkPlayerCaptain(laddClasses.Player(ctx.author.id, "", 0))
    if not isCpt:
        flag = False
        return 1
    else:
        dbCpt = await syncDBHandler.db_getPlayer(ctx.author.id)
        dbPlayer = await syncDBHandler.db_getPlayer(player.id)
        if dbPlayer is None:
            flag = False
            return 2
        else:
            if dbPlayer.teamid!=dbCpt.teamid and dbCpt.teaim!=0:
                flag = False
                return 3
    if flag:
        theTeam = await syncDBHandler.db_getTeam(dbCpt.teamid)
        await syncDBHandler.db_newTeamCapt(theTeam, dbPlayer)
        return 0


# --------------------------------------------------------------------------------------
# Match handling command functions
# --------------------------------------------------------------------------------------
async def challengeTeam(localBOT: commands.Bot, ctx: discord.ApplicationContext, teamid: int):
    """ Handle team challenge. """
    # Check challengeder and teams in DB
    flag = True
    cpt1 = await syncDBHandler.db_getPlayer(ctx.author.id)
    if (await syncDBHandler.db_checkPlayerCaptain(cpt1)):
        teamOne = await syncDBHandler.db_getTeam(cpt1.teamid)
        teamTwo = await syncDBHandler.db_getTeam(teamid)
        if teamOne is None or teamTwo is None:
            flag = False
            return 2
        else:
            # Check conditions if teams are challengeable
            flag = await checkChallengeableTeams(teamOne, teamTwo)
            if not flag:
                return 3
    else:
        flag = False
        return 1

    # Handle challenge invitation
    if flag:
        cpt2 = await syncDBHandler.db_getPlayer(teamTwo.captain)
        theMatch = await syncDBHandler.db_initMatch(teamOne.teamid, teamTwo.teamid)
        theMatch.cdate = datetime.today().strftime("%d-%m-%y")
        await syncDBHandler.db_updateMatch(theMatch)
        theEmbed = boeds.getMatchEmbed(localBOT, theMatch, cpt1, cpt2, teamOne, teamTwo)
        theView = MatchViewST1(localBOT, cpt1, cpt2, teamOne, teamTwo, theMatch)
        theMsg = await ctx.send(embed=theEmbed, view=theView)
    return 0


#--------------------------------------------------------------------------------------
# VIEWS to resolve interactions
#--------------------------------------------------------------------------------------
""" Discord VIEW to handle interaction of player invited to a team. """
class PlayerInviteView(discord.ui.View):
    def __init__(self, lbot: commands.Bot, player: laddClasses.Player, invitingPl: laddClasses.Player, team: laddClasses.Team):
        super().__init__(timeout=None)
        self.player = player
        self.inviting = invitingPl
        self.team = team
        self.bot = lbot

    @discord.ui.button(style=discord.ButtonStyle.green, custom_id="player_accept_invitation", label="Accept")
    async def greenButton(self, button: discord.ui.Button, interact: discord.Interaction):
        author = self.bot.get_guild(laddConfig.LadderInt).get_member(interact.user.id)
        if author.id == self.player.playerid:
            await syncDBHandler.db_addPlayerToTeam(self.player, self.team)
            await updateTeamRosters(discord.ApplicationContext(self.bot, interact), self.bot)
            await interact.message.delete()
        else:
            await interact.response.pong()
        return None

    @discord.ui.button(style=discord.ButtonStyle.red, custom_id="player_reject_invitation", label="Reject")
    async def redButton(self, button: discord.ui.Button, interact: discord.Interaction):
        author = self.bot.get_guild(laddConfig.LadderInt).get_member(interact.user.id)
        if author.id == self.player.playerid | author.id == self.inviting.playerid:
            self.player.teamid = 0
            self.player.joindate = ""
            await syncDBHandler.db_updatePlayer(self.player)
            await interact.message.delete()
        else:
            await interact.response.pong()
        return None


class MatchViewST1(discord.ui.View):
    """ VIEW to handle match challenge acceptance/rejection (stage 1 of match). """
    def __init__(self, lbot: commands.Bot, cpt1: laddClasses.Player, cpt2: laddClasses.Player, 
                team1: laddClasses.Team, team2: laddClasses.Team, match: laddClasses.Match):
        super().__init__(timeout=None)
        self.bot = lbot
        self.cpt1 = cpt1
        self.cpt2 = cpt2
        self.team1 = team1
        self.team2 = team2
        self.match = match

    @discord.ui.button(style=discord.ButtonStyle.green, custom_id="match_stg1_invitation_acc", label="Accept")
    async def greenButton(self, button: discord.ui.Button, interact: discord.Interaction):
        """ Resole accepted challenge """
        author = self.bot.get_guild(laddConfig.LadderInt).get_member(interact.user.id)
        if author.id == self.cpt2.playerid:
            # Update match status and generate all necessary info for map banning
            self.match.status += 1
            await syncDBHandler.db_updateMatch(self.match)
            theMaps = [tmap for tmap in laddConfig.MapList]
            lastBanned = ""
            currBanCpt = self.cpt1
            theEmbed = boeds.getMatchEmbed(self.bot, self.match, self.cpt1, self.cpt2, self.team1, self.team2,
                                            theMaps, currBanCpt, lastBanned)
            theView = MatchViewST2(self.bot, self.cpt1, self.cpt2, self.team1, self.team2, self.match, theMaps,
                                    lastBanned, currBanCpt)
        else:
            theEmbed = boeds.getMatchEmbed(self.bot, self.match, self.cpt1, self.cpt2, self.team1, self.team2)
            theView = MatchViewST1(self.bot, self.cpt1, self.cpt2, self.team1, self.team2, self.match)
        await interact.response.edit_message(embed=theEmbed, view=theView)
        return None
            

    @discord.ui.button(style=discord.ButtonStyle.red, custom_id="match_stg1_invitation_rej", label="Reject")
    async def redButton(self, button: discord.ui.Button, interact: discord.Interaction):
        """ Resole rejected challenge """
        author = self.bot.get_guild(laddConfig.LadderInt).get_member(interact.user.id)
        if author.id == self.cpt1.playerid or author.id == self.cpt2.playerid:
            self.match.status = 101
            await syncDBHandler.db_updateMatch(self.match)
            embedHead = f"Ladder Match : {self.team1.name}' {self.team1.loc} " + \
                        f"vs {self.team2.name}' {self.team2.loc} " + \
                        f"(ID: {self.match.matchid})"
            embedMsg = f"<@{self.cpt2.playerid}> has rejected the challenge."
            theEmbed = boeds.getSingleEmbed(self.bot, embedHead, embedMsg)
            theView = None
        else:
            theEmbed = boeds.getMatchEmbed(self.bot, self.match, self.cpt1, self.cpt2, self.team1, self.team2)
            theView = MatchViewST1(self.bot, self.cpt1, self.cpt2, self.team1, self.team2, self.match)
        await interact.response.edit_message(embed=theEmbed, view=theView)
        return None


class MatchListDropdown(discord.ui.Select):
    """ Generate dropbown list of maps and resolution of the interaction. """
    def __init__(self, lbot: commands.Bot, cpt1: laddClasses.Player, cpt2: laddClasses.Player, 
                team1: laddClasses.Team, team2: laddClasses.Team, match: laddClasses.Match, 
                maps: list, lastban: str, currBanCap: laddClasses.Player):
        super().__init__(
            placeholder="Select a map to ban",
            min_values=1,
            max_values=1,
            options=[],
        )
        self.bot = lbot
        self.cpt1 = cpt1
        self.cpt2 = cpt2
        self.team1 = team1
        self.team2 = team2
        self.match = match
        self.maplist = maps
        self.lastban = lastban
        self.currCpt = currBanCap
        for map in maps:
            self.add_option(label=map, value=map)

    async def callback(self, interact: discord.Interaction):
        author = self.bot.get_guild(laddConfig.LadderInt).get_member(interact.user.id)
        if author.id==self.currCpt.playerid:
            self.lastban = self.values[0]
            self.maplist.remove(self.lastban)
            if len(self.maplist)>3:
                # Call for next captain to pick
                if self.currCpt.playerid == self.cpt1.playerid:
                    self.currCpt = self.cpt2
                else:
                    self.currCpt = self.cpt1
                theEmbed = boeds.getMatchEmbed(self.bot, self.match, self.cpt1, self.cpt2, self.team1, self.team2,
                                            self.maplist, self.currCpt, self.lastban)
                theView = MatchViewST2(self.bot, self.cpt1, self.cpt2, self.team1, self.team2, self.match,
                                        self.maplist, self.lastban, self.currCpt)
                await interact.response.edit_message(embed=theEmbed, view=theView)
            else:
                # Pick random map
                self.match.map = random.choice(self.maplist)
                self.match.status += 1
                await syncDBHandler.db_updateMatch(self.match)
                theEmbed = boeds.getMatchEmbed(self.bot, self.match, self.cpt1, self.cpt2, self.team1, self.team2,
                                            self.maplist, self.currCpt, self.lastban)
                theView = MatchViewST3(self.bot, self.cpt1, self.cpt2, self.team1, self.team2, self.match)
                await interact.response.edit_message(embed=theEmbed, view=theView)
        else:
            # Repost same choice
            theEmbed = boeds.getMatchEmbed(self.bot, self.match, self.cpt1, self.cpt2, self.team1, self.team2,
                                            self.maplist, self.currCpt, self.lastban)
            theView = MatchViewST2(self.bot, self.cpt1, self.cpt2, self.team1, self.team2, self.match,
                                            self.maplist, self.lastban, self.currCpt)
            await interact.response.edit_message(embed=theEmbed, view=theView)


class MatchViewST2(discord.ui.View):
    """ VIEW to handle map ban interactions (stage 2 of match). """
    def __init__(self, lbot: commands.Bot, cpt1: laddClasses.Player, cpt2: laddClasses.Player, 
                team1: laddClasses.Team, team2: laddClasses.Team, match: laddClasses.Match, 
                maps: list,lastban: str, currBanCap: laddClasses.Player):
        super().__init__(timeout=None)
        self.add_item(MatchListDropdown(lbot, cpt1, cpt2, team1, team2, match, maps, lastban, currBanCap))


class MatchViewST3(discord.ui.View):
    """ VIEW to handle match result reporting (stage 3 of match). """
    def __init__(self, lbot: commands.Bot, cpt1: laddClasses.Player, cpt2: laddClasses.Player, 
                team1: laddClasses.Team, team2: laddClasses.Team, match: laddClasses.Match,
                reacts: dict=None, adminresolve: int=0):
        super().__init__(timeout=None)
        self.bot = lbot
        self.cpt1 = cpt1
        self.cpt2 = cpt2
        self.team1 = team1
        self.team2 = team2
        self.match = match
        self.resolver = adminresolve
        if reacts == None:
            self.reacts = {self.cpt1.playerid: None, self.cpt2.playerid: None}
        else:
            self.reacts = reacts
        
        # Add buttons
        self.team1lbl = f"{self.team1.name} wins"
        self.team2lbl = f"{self.team2.name} wins"
        Button1 = discord.ui.Button(style=discord.ButtonStyle.green, label=self.team1lbl, custom_id=match_rep_cid_green)
        Button1.callback = self.callback
        Button2 = discord.ui.Button(style=discord.ButtonStyle.gray, label=f"Draw", custom_id=match_rep_cid_gray)
        Button2.callback = self.callback
        Button3 = discord.ui.Button(style=discord.ButtonStyle.blurple, label=self.team2lbl, custom_id=match_rep_cid_blue)
        Button3.callback = self.callback
        Button4 = discord.ui.Button(style=discord.ButtonStyle.red, label=f"Cancel match", custom_id=match_rep_cid_red)
        Button4.callback = self.callback
        for but in [Button1, Button2, Button3, Button4]:
            self.add_item(but)
        
        self.msgcontent = ""

    async def callback(self, interact: discord.Interaction):
        """ Resolve interaction for reported results. """
        author = self.bot.get_guild(laddConfig.LadderInt).get_member(interact.user.id)
        if author.id in [self.cpt1.playerid, self.cpt2.playerid]:
            self.reacts[author.id] = interact.data['custom_id']
            msgcontent = self.msgcontent

            if (self.reacts[self.cpt1.playerid] is None) | (self.reacts[self.cpt2.playerid] is None):
                # Only one captain responded
                other = set([self.cpt1.playerid, self.cpt2.playerid]) - set([author.id])
                lbl = getReportedResultLbl(self.team1, self.team2, self.reacts[author.id])
                msgcontent = f"<@{author.id}> has reported `{lbl}`." + \
                             f"<@{list(other)[0]}>, please confirm this."
            else:
                # Both captains responded
                if self.reacts[self.cpt1.playerid]==self.reacts[self.cpt2.playerid]:
                    #Captains report same
                    if self.reacts[self.cpt1.playerid]==match_rep_cid_green:
                        msgcontent = f"{self.team1.name} vs {self.team2.name} on {self.match.map}. Winner is **{self.team1.name}**"
                    elif self.reacts[self.cpt1.playerid]==match_rep_cid_blue:
                        msgcontent = f"{self.team2.name} vs {self.team2.name} on {self.match.map}. Winner is **{self.team1.name}**"
                    elif self.reacts[self.cpt1.playerid]==match_rep_cid_gray:
                        msgcontent = f"{self.team2.name} vs {self.team2.name} on {self.match.map}.**The match is a draw.**"
                    else:
                        msgcontent = f"{self.team2.name} vs {self.team2.name} on {self.match.map}.**The match is cancelled.**"
                else:
                    #Captains report different
                    self.resolver = 1
                    lbl1 = getReportedResultLbl(self.team1, self.team2, self.reacts[self.cpt1.playerid])
                    lbl2 = getReportedResultLbl(self.team1, self.team2, self.reacts[self.cpt2.playerid])
                    msgcontent = f"Conflict. <@{self.cpt1.playerid}> reported '{lbl1}' " + \
                                f"while <@{self.cpt2.playerid}> reported '{lbl2}'. " + \
                                f"Admin is needed to resolve the issue."
                    theView = MatchViewST3(self.bot, self.cpt1, self.cpt2, self.team1, self.team2, self.match, self.reacts, self.resolver)
        else:
            # Admin (other than any of the captains) resolves conflict
            if laddConfig.AdminRole in [role.id for role in author.roles] and \
                not(self.reacts[self.cpt1.playerid] is None) and not(self.reacts[self.cpt2.playerid] is None):
                # Evaluate admin decision
                self.reacts[self.cpt1.playerid] = interact.data['custom_id']
                self.reacts[self.cpt2.playerid] = interact.data['custom_id']
                msgconent = f"Admin <@{author.id}> resolves conflict. "
                if self.reacts[self.cpt1.playerid]==match_rep_cid_green:
                    msgcontent = f"{self.team1.name} vs {self.team2.name} on {self.match.map}. Winner is **{self.team1.name}**"
                elif self.reacts[self.cpt1.playerid]==match_rep_cid_blue:
                    msgcontent = f"{self.team2.name} vs {self.team2.name} on {self.match.map}. Winner is **{self.team1.name}**"
                elif self.reacts[self.cpt1.playerid]==match_rep_cid_gray:
                    msgcontent = f"{self.team2.name} vs {self.team2.name} on {self.match.map}.**The match is a draw.**"
                else:
                    msgcontent = f"{self.team2.name} vs {self.team2.name} on {self.match.map}.**The match is cancelled.**"
        
        self.msgcontent = msgcontent
        if self.reacts[self.cpt1.playerid]==self.reacts[self.cpt2.playerid] and not(self.reacts[self.cpt1.playerid] is None):
            theView = None
            theEmbed = boeds.getMatchEmbed(self.bot, self.match, self.cpt1, self.cpt2, self.team1, self.team2, reportMsg=self.msgcontent)
            await interact.message.edit(embed=theEmbed, view=theView)
            await self.resolveresult(interact, self.reacts[self.cpt1.playerid], self.resolver)
        else:
            theView = MatchViewST3(self.bot, self.cpt1, self.cpt2, self.team1, self.team2, self.match, self.reacts, self.resolver)
            theEmbed = boeds.getMatchEmbed(self.bot, self.match, self.cpt1, self.cpt2, self.team1, self.team2, reportMsg=self.msgcontent)
            await interact.response.edit_message(embed=theEmbed, view=theView)
        

    async def resolveresult(self, interact: discord.Interaction, resultmsg: str, resolver: int=0):
        """ Function to evaluate match result and update match status. """

        if self.reacts[self.cpt1.playerid]==match_rep_cid_red:
            # Match is cancelled
            self.match.status = 102
            await syncDBHandler.db_updateMatch(self.match)
        else:
            # Update match result when played and update player ELO-s
            recentteam1 = await syncDBHandler.db_getTeam(self.team1.teamid)
            recentteam2 = await syncDBHandler.db_getTeam(self.team2.teamid)
            self.team1.win = recentteam1.win
            self.team1.draw = recentteam1.draw
            self.team1.loss = recentteam1.loss
            self.team2.win = recentteam2.win
            self.team2.draw = recentteam2.draw
            self.team2.loss = recentteam2.loss
            self.match.status += 1
            await syncDBHandler.db_updateMatch(self.match)
            if self.reacts[self.cpt1.playerid]==match_rep_cid_green:
                result = [0,1]
                self.team1.win += 1
                self.team2.loss += 1
                self.outcome = 1
            elif self.reacts[self.cpt1.playerid]==match_rep_cid_blue:
                result = [1,0]
                self.team1.loss += 1
                self.team2.win += 1
                self.outcome = 3
            else:
                result = [0,0]
                self.team1.draw += 1
                self.team2.draw += 1
                self.outcome = 2

            # Retrieve and update new elo ratings
            teamOneRoster = await syncDBHandler.db_getTeamRoster(self.team1)
            teamTwoRoster = await syncDBHandler.db_getTeamRoster(self.team2)
            ratingTeamOne = [p.getEloRating() for p in teamOneRoster[0: min(len(teamOneRoster),5)] ]
            ratingTeamTwo = [p.getEloRating() for p in teamTwoRoster[0: min(len(teamTwoRoster),5)] ]
            [newOneRatingraw, newTwoRatingraw] = trueskill.rate([ratingTeamOne, ratingTeamTwo], ranks=result)
            newOneRating = [newOneRatingraw[0] for i in range(len(teamOneRoster))]
            newTwoRating = [newTwoRatingraw[0]for i in range(len(teamTwoRoster))]
            teams = [self.team1, self.team2]
            rosters = [teamOneRoster, teamTwoRoster]
            newRatings = [newOneRating, newTwoRating]
            for t in range(2):
                teamElo = []
                for j in range(len(rosters[t])):
                    player = rosters[t][j]
                    player.elo = player.encodeEloString(newRatings[t][j])
                    teamElo.append(newRatings[t][j].mu)
                    await syncDBHandler.db_updatePlayer(player)
                teams[t].elo = sum(teamElo) / len(teamElo)
                await syncDBHandler.db_updateTeam(teams[t])
              
        theEmbed = boeds.getMatchEmbed(self.bot, self.match, self.cpt1, self.cpt2, self.team1, self.team2, 
                                reportMsg=self.msgcontent, result=self.outcome, resolver=self.resolver)
        await interact.response.edit_message(embed=theEmbed, view=None)
        return None